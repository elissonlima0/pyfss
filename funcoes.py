###############################################################################################
# Primeira prática Computação Natural - PSO
# Autor: Elisson Lima
# Arquivo de definição de funções do CEC 2013
###############################################################################################
import numpy as np
import math

def sphere(p):
   return np.sum(list(map(lambda x: x**2, np.array(p, dtype=np.float64))))


def rastrigin(ponto):
    return np.sum(list(map(lambda x : x**2 - 10 * math.cos(2 * math.pi * x) + 10, ponto)))


def rosenbrock(x):

    total = 0

    for i in range(len(x)-1):

        total += 100*((x[i+1] - x[i]**2)**2) + (x[i] - 1)**2

    return total