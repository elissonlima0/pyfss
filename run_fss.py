from FSS import FSS
import funcoes as func
import matplotlib.pyplot as plt

fss = FSS(30, 30, func.rosenbrock, initial_pos=(15,30))
result = fss.run_fss(500000)

x = [i for i in range(len(result))]
plt.plot(x, result, 'r-', label='global')
plt.show()

