import numpy as np
from scipy.spatial import distance
import time
import sys

class FSS:

    def __init__(self, school_size, dim_size, func, initial_pos=(0,1),
                 step_ind_ini = 0.1, step_ind_final = 0.001,
                 step_vol_ini = 0.01, step_vol_final = 0.001):

        print("Starting FSS....")
        ###Initializing step values
        self.step_ind = step_ind_ini
        self.step_ind_ini = step_ind_ini
        self.step_ind_final = step_ind_final
        self.step_vol = step_vol_ini
        self.step_vol_ini = step_vol_ini
        self.step_vol_final = step_vol_final

        self.vfunc = np.vectorize(func, signature='(m)->()')
        self.euclidean = np.vectorize(distance.euclidean)
        self.fitness_eval = 0

        ###Initializing schools randomly through initial_pos
        self.fishes = (initial_pos[1] - initial_pos[0]) * \
                  np.random.random_sample((school_size, dim_size))  + initial_pos[0]
        self.weights = np.ones((school_size,))
        self.delta = np.zeros((school_size,))
        self.delta_pos = np.zeros((school_size, dim_size))


    def individual_movement(self):

        neighbor_pos = self.fishes + (self.step_ind * np.random.uniform(-1,1, self.fishes.shape))
        delta = self.vfunc(neighbor_pos) - self.vfunc(self.fishes)
        self.fitness_eval += 2 * self.fishes.shape[0]

        ###Searching for which fish is successful
        idx = np.nonzero(delta < 0)
        self.delta[idx] = np.absolute(delta[idx])
        self.delta_pos[idx] = neighbor_pos[idx] - self.fishes[idx]
        self.fishes[idx] = neighbor_pos[idx]

        ##Feeding sucessuful fishes
        self.weights[idx] = self.weights[idx] + (self.delta[idx] / self.delta.max())


    def instinctive_movement(self):
        idx = np.nonzero(self.delta > 0)
        self.fishes[idx] = self.fishes[idx] + (self.delta_pos[idx] + self.delta[idx].reshape(-1,1)) / np.sum(self.delta)


    def volitive_movement(self, prev_weight):

        barycenter = self.fishes * self.weights / np.sum(self.weights)
        term = (self.fishes - barycenter) / self.euclidean(self.fishes, barycenter)

        weight_sum = np.sum(self.weights)

        if weight_sum < prev_weight:
            self.fishes = self.fishes - (self.step_vol * term)
        elif weight_sum > prev_weight:
            self.fishes = self.fishes + (self.step_vol * term)


    def update_steps(self, iterations):
        self.step_ind = self.step_ind - ((self.step_ind_ini - self.step_ind_final) / iterations)
        self.step_vol = self.step_vol - ((self.step_vol_ini - self.step_vol_final) / iterations)

    def run_fss(self, fitness_evals):

        iterations = 1
        fits = []
        buf_time = 0

        while self.fitness_eval < fitness_evals:

            start_time = time.time()

            prev_weight = np.sum(self.weights)
            self.individual_movement()
            self.instinctive_movement()
            self.volitive_movement(prev_weight)
            self.update_steps(iterations)

            fits.append(np.array([self.vfunc(i) for i in self.fishes]).min())

            elapsed_time = time.time() - start_time
            buf_time += elapsed_time
            sys.stdout.write("\r")
            pstr = "Iteração - Qtd Evaluates [" + str(iterations) + " - " + str(self.fitness_eval) + "] Tempo: " + \
                   time.strftime("%H:%M:%S", time.gmtime(buf_time))
            sys.stdout.write(pstr)
            sys.stdout.flush()

            iterations +=1

        return fits
